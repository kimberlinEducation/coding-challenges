# Coding challenge for Full Stack Developer

## Purpose
Aim of these tests are to:

*	evaluate your software engineering capability
*	evaluate your strength building and managing APIs
*	benchmark your technical experience and maturity
*	understand how you design and implement your solution
*	we are not evaluating design expertise so no need to spend a lot of time on UI but adding UI libraries such as bootstrap, Vuetify etc.. is welcome

## How you will be judged
You will be scored on:

* coding style, comments, and logging (20%)
* design patterns and algorithms (20%)
* solution design - structure and quality (20%)
* use of source control and documentation (20%)
* considerations for CI/CD/DevExp (10%)
* one of each unit/functional/E2E tests (10%)

## Instructions

Build an application with a front and backend that allows following behaviours. You can find a rough user interface of what we are looking for [here](pdf/UserInterfaceEventTest.pdf):

1. Login functionality into an authenticated page (no need to build registration and logout)
2. An authenticated page which shows a list of events
3. Search for a event by title keyword and filter via types
4. Click on an event and book based on a time slot
5. Booking needs to be linked to authenticated user and saved on the db

## Data

An event is made up of the following:

| Columns       | Description                                                                                    |
|---------------|------------------------------------------------------------------------------------------------|
| title         | String                                                                                         |
| description   | Hml                                                                                           |
| image         | Image url string                                                                               |
| date          | Date                                                                                           |
| types         | Event can be many of ("virtual classroom", "food & drink", "charity & causes", "free", "paid") |
| booking_times | Event can be at any time of the day                                                            |

Sample data is availailable for you to use [here](data/event_data.json)

## Architecture

1. Splitting the projects as SPA & API is preferred
2. You can use any language for FE (Vue JS or pure ES6 preferred) and BE (Laravel or headless CMS)
3. Add your app as Github repo and send us the link for the code review
4. Add a `Readme.md` file in your repository with instructions to build the app locally. (not too detailed we are set up for running npm and PHP projects)

Suggested time: 4 hours

## Finally
Once you finished the test, either send us a pull request or URL for your Github or Bitbucket repository. Questions email brice@kimberlineducation.com.au