# A set of coding challenges for engineering roles at Kimberlin Education

## Purpose
Aim of these tests are to,

- evaluate your software engineering capability
- benchmark your technical experience and maturity
- understand how you design and implement your solution

## How you will be judged
You will be scored on,

- coding style, comments, and logging (20%)
- design patterns and algorithms (20%)
- solution design - structure and quality (20%)
- use of source control and documentation (20%)
- considerations for CI/CD/DevExp (10%)
- at least one basic unit/functional/E2E tests (10%)

## Tests

- [Full-Stack Developer](full-stack-developer.md)

## Finally
Once you finished the test, either send us a pull request or URL for your Github repository. Questions email brice@kimberlineducation.com.au.